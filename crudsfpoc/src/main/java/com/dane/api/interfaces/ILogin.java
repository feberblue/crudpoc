package com.dane.api.interfaces;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.dane.api.model.Customer;

public interface ILogin extends CrudRepository<Customer, Long>{

	@Query("select c from Customer c where c.identificacion=?1")
	Customer findByIdentification(String identification);
}
