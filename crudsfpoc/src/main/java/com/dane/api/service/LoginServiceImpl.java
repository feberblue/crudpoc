package com.dane.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dane.api.interfaces.ILogin;
import com.dane.api.model.Customer;
import com.dane.api.model.Login;

@Service
public class LoginServiceImpl implements ILoginService{

	@Autowired
	private ILogin data;
	
	@Override
	public Customer login(Login login) {
		// TODO Auto-generated method stub
		String usuario = login.getUsuario();
		String password = login.getPassword();
		Customer loginCustomer = null;
		// comparación solamente para este caso el lo
		if(usuario.equals(password)) {
			loginCustomer = data.findByIdentification(password);
		}
		return loginCustomer;
	}

}
