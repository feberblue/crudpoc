package com.dane.api.service;

import com.dane.api.model.Customer;
import com.dane.api.model.Login;

public interface ILoginService {
	
	public Customer login(Login login);

}
