package com.dane.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class Customer {
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 30, nullable = false)
	private String primernombre;
	
	@Column(length = 30, nullable = true)
	private String segundonombre;
	
	@Column(length = 30, nullable = false)
	private String primerapellido;
	
	@Column(length = 30, nullable = false)
	private String segundoapellido;
	
	@Column(length = 20, nullable = false, unique = true)
	private String identificacion;
	
	@Column(length = 3, nullable = true)
	private int edad;
	
	@Column(length = 1, nullable = false)
	private String sexo;
	
	@Column(length = 200, nullable = false, unique = true)
	private String email;
	
	@Column(length = 20, nullable = false)
	private String telefono;
	
	@Column(length = 1, nullable = false, columnDefinition = "varchar(1) default 'A'" )
	private String activo;
	
	public Customer() {
		
	}

	
	public Customer(String primernombre, String segundonombre, String primerapellido, String segundoapellido,
			String identificacion, int edad, String sexo, String email, String telefono, String activo) {
		super();
		this.primernombre = primernombre;
		this.segundonombre = segundonombre;
		this.primerapellido = primerapellido;
		this.segundoapellido = segundoapellido;
		this.identificacion = identificacion;
		this.edad = edad;
		this.sexo = sexo;
		this.email = email;
		this.telefono = telefono;
		this.activo = activo;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPrimernombre() {
		return primernombre;
	}


	public void setPrimernombre(String primernombre) {
		this.primernombre = primernombre;
	}


	public String getSegundonombre() {
		return segundonombre;
	}


	public void setSegundonombre(String segundonombre) {
		this.segundonombre = segundonombre;
	}


	public String getPrimerapellido() {
		return primerapellido;
	}


	public void setPrimerapellido(String primerapellido) {
		this.primerapellido = primerapellido;
	}


	public String getSegundoapellido() {
		return segundoapellido;
	}


	public void setSegundoapellido(String segundoapellido) {
		this.segundoapellido = segundoapellido;
	}


	public String getIdentificacion() {
		return identificacion;
	}


	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public String getSexo() {
		return sexo;
	}


	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getActivo() {
		return activo;
	}


	public void setActivo(String activo) {
		this.activo = activo;
	}


	@Override
	public String toString() {
		return "Customer{id:" + id + ", primernombre:" + primernombre + ", segundonombre:" + segundonombre
				+ ", primerapellido:" + primerapellido + ", segundoapellido:" + segundoapellido + ", identificacion:"
				+ identificacion + ", edad:" + edad + ", sexo:" + sexo + ", email:" + email + ", telefono:" + telefono
				+ "}";
	}


	
	
	

}
