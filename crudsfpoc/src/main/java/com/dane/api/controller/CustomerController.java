package com.dane.api.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dane.api.model.Customer;
import com.dane.api.service.ICustomerService;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping("/api/v1")
public class CustomerController {

	@Autowired
	private ICustomerService service;

	/**
	 * guardar un objeto nuevo en tabla
	 * @param customer
	 * @return
	 */
	@PostMapping("/customer")
	public ResponseEntity<Customer> save(@RequestBody Customer customer) {
		try {
			System.out.println(customer);
			Customer newCustomer = service.save(customer);
			return ResponseEntity.created(new URI("/customer/" + newCustomer.getId())).body(newCustomer);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	/**
	 * obtener todos los objetos de tabla
	 * @return
	 */
	@GetMapping("/customer")
	public ResponseEntity<List<Customer>> findAll() {
		return ResponseEntity.ok(service.findAll());
	}

	/**
	 * Buscar objeto por su identificador
	 * @param id
	 * @return
	 */
	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> findById(@PathVariable Long id) {
		return ResponseEntity.ok(service.findById(id));
	}

	/**
	 * Actualizar un registro de la tabla
	 * @param customer objeto a cambiar
	 * @param id identificador a buscar 
	 * @return objeto actalizado
	 */
	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> update(@RequestBody Customer customer, @PathVariable Long id) {
		Customer customerFind = service.findById(id);
		customerFind.setPrimerapellido(customer.getPrimerapellido());
		customerFind.setSegundoapellido(customer.getSegundoapellido());
		customerFind.setPrimernombre(customer.getPrimernombre());
		customerFind.setSegundonombre(customer.getSegundonombre());
		customerFind.setIdentificacion(customer.getIdentificacion());
		customerFind.setEdad(customer.getEdad());
		customerFind.setSexo(customer.getSexo());
		customerFind.setEmail(customer.getEmail());
		customerFind.setTelefono(customer.getTelefono());
		// customerFind.setActivo(customer.getActivo());

		return ResponseEntity.ok(service.save(customerFind));
	}

	/**
	 * Permite Activar o desactivar un objeto
	 * 
	 * @param customer {solo el campo activo: (A: Active | I: Inactive)}
	 * @param id
	 * @return
	 */
	@PutMapping("/customer/activate/{id}")
	public ResponseEntity<Customer> activate(@RequestBody Customer customer, @PathVariable Long id) {
		Customer customerFind = service.findById(id);
		customerFind.setActivo(customer.getActivo());
		return ResponseEntity.ok(service.save(customerFind));
	}

	/**
	 * Eliminar un registro de la tabla
	 * @param id
	 * @return
	 */
	@DeleteMapping("/customer/{id}")
	public Map<String, Boolean> delete(@PathVariable Long id) {
		Map<String, Boolean> response = new HashMap<>();
		Customer customerFind = service.findById(id);
		if (customerFind != null) {
			service.delete(id);
			response.put("deleted", Boolean.TRUE);
		} else {
			response.put("deleted", Boolean.FALSE);
		}
		return response;
	}
}
