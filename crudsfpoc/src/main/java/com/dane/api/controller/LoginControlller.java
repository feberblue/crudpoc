package com.dane.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dane.api.model.Customer;
import com.dane.api.model.Login;
import com.dane.api.service.ILoginService;

@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
public class LoginControlller {
	
	@Autowired
	private ILoginService service;
	
	
	@PostMapping("/login")
	public ResponseEntity<Customer> Login(@RequestBody Login login) {
		return ResponseEntity.ok(service.login(login));
	}
}
