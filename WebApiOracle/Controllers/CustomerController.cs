﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Interface;
using WebApiOracle.Models;
using WebApiOracle.Utility;

namespace WebApiOracle.Controllers
{
    [Route("api/v1/[controller]")]
    [Produces("application/json")]    
    [ApiController]
    public class CustomerController : ControllerBase
    {
        // Se inyecta el servicio mediante la referecnia de la interfaz
        ICustomerService service;

        public CustomerController(ICustomerService customerService) {
            service = customerService;
        }


        // Ejemplo exclusivo del Uso de QueryBulder class
        [HttpGet] 
        [Route("all")]
        public Object GetAll() {
            return QueryBuilder<Customer>.GetList(new Customer());
        }

        // Ejemplo extrayendo la informacion de customer mediante la interfaz 
        [HttpGet]
        public ActionResult GetAllCustomer() {
            // return service.GetAllCustomers();
            var result = service.GetListCustomer();
            if (result == null) {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet("{id}")]
        public ActionResult GetCustomerById(int id)
        {
            var result= service.GetCustomerByIdPrincipal(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);

        }

        [HttpPut("{id}")]
        public ActionResult UpdateCustomer(int id, Customer customer)
        {
            var resp = new Dictionary<string, bool>();
            try
            {
                service.EditCustomer(customer, id);
                resp.Add("updated", true);                
            }
            catch (Exception)
            {
                resp.Add("updated", false);
            }

            return Ok(resp);

        }

        [HttpDelete("{id}")]
        public ActionResult DeleteCustomer(int id)
        {
            var resp = new Dictionary<string, bool>();
            try
            {
                var customer = service.GetCustomerByIdPrincipal(id);
                Customer custom = (Customer)customer;
                service.DeleteCustomer(custom.Id);                
                resp.Add("deleted", true);
            }
            catch (Exception)
            {                
                resp.Add("deleted", false);                
            }
            return Ok(resp);

        }

        [HttpPost]
        public ActionResult create(Customer customer) {
            var resp = new Dictionary<string, bool>();
            try
            {
                if (customer.Id != null && customer.Id>0) {
                    service.EditCustomer(customer, customer.Id.Value);
                    resp.Add("updated", true);
                }
                else
                {
                    service.AddCustomer(customer);
                    resp.Add("created", true);
                }                                              
            }
            catch (Exception)
            {
                resp.Add("created", false);
            }
            return Ok(resp);

        }
    }
}
