﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Models;

namespace WebApiOracle.Interface
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAllCustomers();

        object GetCustomerByIdPrincipal(int id);

        int AddCustomer(Customer customer);

        void EditCustomer(Customer customer, int id);

        void DeleteCustomer(Int32? id);

        object GetListCustomer();
    }
}
