﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Models;

namespace WebApiOracle.Repositoty
{
    public interface ICustomerRepository
    {
        object GetAllCustomer();

        object GetCustomerById(int idCustomer);
        
    }
}
