﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Interface;
using WebApiOracle.Models;

namespace WebApiOracle.Services
{
    public class CustomerService : ICustomerService
    {

        private readonly string _connectionString;

        // IConfiguration configuration;

        public CustomerService(IConfiguration _configuration) {
            _connectionString = _configuration.GetConnectionString("OracleDBConnection");
        }

        public IDbConnection GetConnection() {            
            var conn = new OracleConnection(_connectionString);
            return conn;
        }

        public object GetListCustomer() {
            object result = null;
            try
            {
                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed) {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open) {
                    var query = "SELECT ID,PRIMERNOMBRE,SEGUNDONOMBRE,PRIMERAPELLIDO,SEGUNDOAPELLIDO,IDENTIFICACION,EDAD,EMAIL,SEXO,TELEFONO,ACTIVO FROM CUSTOMER ";
                    result = SqlMapper.Query<Customer>(conn, query, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            List<Customer> listCustomer = new List<Customer>();
            try
            {
                using (OracleConnection con = new OracleConnection(_connectionString))
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        con.Open();
                        //cmd.BindByName = true;
                        //cmd.CommandText = "SELECT ID,PRIMERNOMBRE,SEGUNDONOMBRE,PRIMERAPELLIDO,SEGUNDOAPELLIDO,IDENTIFICACION,EDAD,EMAIL,SEXO,TELEFONO,ACTIVO FROM CUSTOMER ";                                       
                        cmd.CommandText = "SELECT * FROM Customer";

                        OracleDataReader dReader = cmd.ExecuteReader();

                        while (dReader.Read())
                        {
                            Customer custon = new Customer()
                            {
                                Id = Convert.ToInt32(dReader["ID"]),
                                Primernombre = dReader["PRIMERNOMBRE"].ToString(),
                                Segundonombre = dReader["SEGUNDONOMBRE"].ToString(),
                                Primerapellido = dReader["PRIMERAPELLIDO"].ToString(),
                                Segundoapellido = dReader["SEGUNDOAPELLIDO"].ToString(),
                                Identificacion = dReader["IDENTIFICACION"].ToString(),
                                Sexo = (char)dReader["SEXO"],
                                Edad = Convert.ToInt32(dReader["EDAD"]),
                                Email = dReader["EMAIL"].ToString(),
                                Telefono = dReader["TELEFONO"].ToString(),
                                Activo = (char)dReader["ACTIVO"]
                            };
                            listCustomer.Add(custon);
                        }
                        return listCustomer;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return listCustomer;
            }
        }

        public object GetCustomerByIdPrincipal(int id)
        {
            object findCustomer = null;
            
            try
            {
                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "SELECT ID,PRIMERNOMBRE,SEGUNDONOMBRE,PRIMERAPELLIDO,SEGUNDOAPELLIDO,IDENTIFICACION,EDAD,EMAIL,SEXO,TELEFONO,ACTIVO FROM CUSTOMER WHERE id=:idFund ";
                    var paramset = new OracleParameter(":id", id);
                    findCustomer = SqlMapper.QueryFirstOrDefault<Customer>(conn, query, new { idFund = id }, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return findCustomer;
        }

        public int AddCustomer(Customer customer)
        {
            try
            {
                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "INSERT INTO HR.CUSTOMER " +
                                 "(EDAD, EMAIL, IDENTIFICACION, PRIMERAPELLIDO, PRIMERNOMBRE, SEGUNDOAPELLIDO, SEGUNDONOMBRE, SEXO, TELEFONO)" +
                                 "VALUES(:edadI, :emailI, :identificacionI, :apellido1I, :nombre1I, :apellido2I, :nombre2I, :sexoI, :telefonoI)";

                    var rowsAffects = SqlMapper.Execute(conn, query, new
                    {
                        edadI = customer.Edad,
                        emailI = customer.Email,
                        identificacionI = customer.Identificacion,
                        apellido1I = customer.Primerapellido,
                        nombre1I = customer.Primernombre,
                        apellido2I = customer.Segundoapellido,
                        nombre2I = customer.Segundonombre,
                        sexoI = customer.Sexo,
                        telefonoI = customer.Telefono
                    }, commandType: CommandType.Text);
                    return rowsAffects;
                }
                return 0;
            }
            catch (Exception)
            {
                return -1;
            }
           
        }

        public void DeleteCustomer(Int32? idCustomer)
        {

            try
            {
                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "DELETE FROM HR.CUSTOMER " +
                                            "WHERE ID=:idD";

                    var rowsAffects = SqlMapper.Execute(conn, query, new
                    {                        
                        idD = idCustomer
                    }, commandType: CommandType.Text);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        public void EditCustomer(Customer customer, int id)
        {
            try
            {
                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "UPDATE HR.CUSTOMER SET " +
                                            "EDAD=:edadU, EMAIL=:emailU, IDENTIFICACION=:identificacionU, PRIMERAPELLIDO=:apellido1U, " +
                                            "PRIMERNOMBRE=:nombre1U, SEGUNDOAPELLIDO=:apellido2U, SEGUNDONOMBRE=:nombre2U, SEXO=:sexoU, TELEFONO=:telefonoU " +
                                            "WHERE ID=:idU";

                    var rowsAffects = SqlMapper.Execute(conn, query, new
                    {
                        edadU = customer.Edad,
                        emailU = customer.Email,
                        identificacionU = customer.Identificacion,
                        apellido1U = customer.Primerapellido,
                        nombre1U = customer.Primernombre,
                        apellido2U = customer.Segundoapellido,
                        nombre2U = customer.Segundonombre,
                        sexoU = customer.Sexo,
                        telefonoU = customer.Telefono,
                        idU = id
                    }, commandType: CommandType.Text);
                    
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }       
    }
}
