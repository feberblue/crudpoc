﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WebApiOracle.Models
{
    public class Customer
    {
         
        public Int32? Id { get; set; }

        [Required]        
        public string Primernombre { get; set; }
         
        public string Segundonombre { get; set; }

        [Required]         
        public string Primerapellido { get; set; }

        [Required]         
        public string Segundoapellido { get; set; }
         
        public string Identificacion { get; set; }
         
        public int Edad { get; set; }
         
        public char Sexo { get; set; }
         
        public string Email { get; set; }
         
        public string Telefono { get; set; }
         
        public char Activo { get; set; }

    }
}
