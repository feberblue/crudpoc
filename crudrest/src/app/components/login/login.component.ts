import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;

  constructor(private fb: FormBuilder,       
              private router: Router) {
    this.loginForm = this.fb.group({
      usuario: ['', [Validators.required, Validators.email, Validators.maxLength(200)]],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  setLocalStorage(user: any) {
    const usuario: User = {
      uid: user.uid,
      email: user.email
    };

    localStorage.setItem('user', JSON.stringify(usuario));
  }

  login() {
    const usuario = this.loginForm.get('usuario')?.value;
    const password = this.loginForm.get('password')?.value;
    this.loading = true;
    
  }

}
