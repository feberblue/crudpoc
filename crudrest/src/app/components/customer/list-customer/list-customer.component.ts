import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/services/customer.service';


@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.css']
})
export class ListCustomerComponent implements OnInit {

  personaForm: FormGroup;
  personas: any;

  sexos: any;


  constructor(private fb: FormBuilder,
    private customerService: CustomerService) {
    this.personaForm = this.fb.group({
      id: [0],
      primernombre: ['', Validators.required],
      segundonombre: [''],
      primerapellido: ['', Validators.required],
      segundoapellido: ['', Validators.required],
      identificacion: ['', Validators.required],
      edad: [''],
      sexo: ['', Validators.required],
      email: ['', [Validators.email]],
      telefono: ['', [Validators.maxLength(30)]]
    });

    this.sexos = [{
      nombre: "MASCULINO",
      id: "M"
    }, {
      nombre: "FEMENINO",
      id: "F"
    }];
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.customerService.getAllCustomer().subscribe(resp => {
      this.personas = resp;
    },
      error => { console.error(error) }
    );
  }

  guardar(): void {
    
    const idCustomer = this.personaForm.value.id;
    
    if (idCustomer === undefined || idCustomer === null || idCustomer === "" || idCustomer === 0) {
      this.customerService.saveCustomer(this.personaForm.value)
        .subscribe(resp => {
          console.log({resp})
          this.personaForm.reset();
          this.getAll();
        },
          error => { console.error(error) }
        )
    } else {

      this.customerService.updateCustomer(this.personaForm.value, idCustomer + "")
        .subscribe(resp => {
          console.log({ resp });
          this.personaForm.reset();
          this.getAll();
        }, error => {
          console.log("error", { error });
        })
    }


  }

  eliminar(persona: any) {
    this.customerService.deleteCustomer(persona.id).subscribe(resp => {
      console.log(resp);
      if (resp === true) {
        this.personas.pop(persona)
      }
      this.getAll();
    })

  }

  editar(persona: any) {
    this.personaForm.setValue({
      id: persona.id,
      primernombre: persona.primernombre,
      segundonombre: persona.segundonombre,
      primerapellido: persona.primerapellido,
      segundoapellido: persona.segundoapellido,
      identificacion: persona.identificacion,
      edad: persona.edad,
      sexo: persona.sexo,
      email: persona.email,
      telefono: persona.telefono,
    });

    console.log(this.personaForm)
  }

}
