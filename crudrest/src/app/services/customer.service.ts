import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@Angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private API_SERV = "http://localhost:5000/api/v1/customer/";//// "http://localhost:8080/api/v1/customer/";

  constructor(private httpClient: HttpClient) { }

  // obtener todos los customer
  public getAllCustomer(): Observable<any> {
    return this.httpClient.get(this.API_SERV);
  }

  public getCustomerById(id: any): Observable<any> {
    return this.httpClient.get(this.API_SERV + `${id}`)
  }

  public saveCustomer(customer: any): Observable<any> {
    return this.httpClient.post(this.API_SERV, customer);
  }

  public deleteCustomer(id: string): Observable<any> {
    return this.httpClient.delete(this.API_SERV + `${id}`);
  }

  public updateCustomer(customer: any, id: string):Observable<any>{
    return this.httpClient.put(this.API_SERV+`${id}`, customer);
  }


}
