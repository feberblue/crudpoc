# CRUD Personas
## Proyectos

Los proyectos presentes corresponden con la siguiente configuracion de carpetas.

- [crudrest]: proyecto Angular
- [crudsfpoc]: proyecto API REST en spring framework
- [WebApiOracle]: Proyecto API REST en .net core 3.1

# Proyecto crudrest
Ejecutar los pasos para iniciar proyecto despues de realizar el respectivo clonado del repositorio

- npm i
- ng serve --o
- navegar a la ruta [http://localhost:4200](http://localhost:4200)

# Proyecto crudsfpoc

Este proyecto tiene configurado el api documento swagger, el cual permite ver la documentacion de forma grafica del proyecto

Una ver ejecutado dicho proyecto se puede navegar a la siguiente direccion web [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

los servicios son los listados a continuacion
- [http://localhost:8080/api/v1/customer](http://localhost:8080/api/v1/customer) para POST, GET, DELETE, PUT

- [http://localhost:8080/api/v1/login](http://localhost:8080/api/v1/login) POST


# Proyecto WebApiOracle

proyecto con swagger configurado son los mismo servicios que se usan en el proyecto crudsfpoc en la siguiente direccion web [http://localhost:57551/swagger/index.html](http://localhost:57551/swagger/index.html)

los servicios son los listados a continuacion
- [http://localhost:57551/api/v1/customer](http://localhost:57551/api/v1/customer) para POST, GET, DELETE, PUT